﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspectTestFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Program started. Hello from Main()");
            var r = new Runner();
            r.Run(true);
            Console.ReadKey();
        }
    }
}
