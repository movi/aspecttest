﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArxOne.MrAdvice.Advice;

namespace AspectTestFirst.Aspects
{
    public class MyProudAdvice : Attribute, IMethodAdvice
    {
        public void Advise(MethodAdviceContext context)
        {
            Console.WriteLine("Started " +context.TargetType.FullName +"."+context.TargetMethod.Name);
            // do things you want here
            var time = DateTime.Now;
            int index = 0;
            foreach (var argument in context.Arguments)
            {
                Console.Write($"{index}. {argument}");
                index++;
            }
            Console.WriteLine();

            context.Proceed(); // this calls the original method
                               // do other things here
            var span = DateTime.Now - time;

            Console.WriteLine("Ended " + context.TargetMethod.Name);
            Console.WriteLine($"time: {span.Minutes}m {span.Seconds}s {span.Milliseconds} ms");
        }
    }
}
